from django.apps import AppConfig


class AuthknoxConfig(AppConfig):
    name = 'authknox'
