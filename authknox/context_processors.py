from django.conf import settings


def default_domain(request):
    data = {}
    data['DEFAULT_DOMAIN'] = settings.DEFAULT_DOMAIN
    return data
