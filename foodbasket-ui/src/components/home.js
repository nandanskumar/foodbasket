import React, { Component } from "react";


import { connect } from "react-redux";
// MUI Stuff
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";



class Home extends Component {
  constructor() {
    super();
    this.state = {
      loading: false,
      errors: {},
      error: false,
      token: localStorage.getItem("token"),
    };
  }


  render() {

    return (
      <div>

        <Grid container spacing={8}>
          <Typography>Welcome</Typography>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
});


export default connect(
  mapStateToProps,
  null
)((Home));
