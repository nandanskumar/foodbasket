import React, { Component } from "react";
// import { Link } from "react-router-dom";

import { connect } from "react-redux";
import { loginUser } from "../store/actions/users";

// import AppIcon from "../images/logo.jpeg";
// MUI Stuff
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";



class Login extends Component {

  constructor() {
    super();
    this.state = {
      email: "",
      password: "",
      loading: false,
      errors: {},
    };
  }
  handleSubmit = (event) => {
    event.preventDefault();
    this.setState({ loading: !this.state.loading });
    const userData = {
      username: this.state.email,
      password: this.state.password,
    };
    this.props.loginUser(userData, this.props.history);
  };
  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  componentDidUpdate(prevProps) {
    if (prevProps.UI.errors !== this.props.UI.errors) {
      this.setState({ errors: this.props.UI.errors });
    }
  }

  render() {
    const { errors } = this.state;

    return (
      <div style={{ marginTop: 100 }}>
        <Grid container component="main" spacing={10}>
          <Grid item sm />
          <Grid item sm>
            <form noValidate onSubmit={this.handleSubmit}>
              <TextField
                id="email"
                name="email"
                type="email"
                label="Email"
                value={this.state.email}
                onChange={this.handleChange}
                fullWidth
              />
              <TextField
                id="password"
                name="password"
                type="password"
                label="Password"
                value={this.state.password}
                onChange={this.handleChange}
                fullWidth
              />
              <br />
              <br />
              <br />

              {errors && errors.non_field_errors && (
                <Typography variant="body2">
                  {errors.non_field_errors[0]}
                </Typography>
              )}
              <Button
                type="submit"
                variant="contained"
                color="primary"
                disabled={this.props.UI.loading}
                onClick={this.handleSubmit}
              >
                Login
                {this.props.UI.loading && (
                  <CircularProgress size={30} />
                )}
              </Button>
            </form>
          </Grid>

          <Grid item sm />
        </Grid>
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  user: state.user,
  UI: state.UI,
});

const mapActionsToProps = {
  loginUser,
};

export default connect(
  mapStateToProps,
  mapActionsToProps
)(Login);
