import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
// MUI stuff
import AppBar from "@material-ui/core/AppBar";
import Button from "@material-ui/core/Button";
// Icons
// import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import { logoutUser } from "../store/actions/users";

class Navbar extends Component {
  render() {
    return (
      <AppBar>
        <Grid className="header">
          {this.props.user && (
            <Grid item>
              {this.props.user.authenticated && (
                <Button color="inherit" component={Link} to="/home">
                  {"Home"}
                </Button>
              )}

              {this.props.user.authenticated && (
                <Button
                  color="inherit"
                  component={Link}
                  to="/"
                  onClick={() => this.props.logoutUser()}
                >
                  Logout
                </Button>
              )}
            </Grid>
          )}
        </Grid>
      </AppBar>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
});

const mapActionsToProps = {
  logoutUser,
};

export default connect(
  mapStateToProps,
  mapActionsToProps
)(Navbar);
