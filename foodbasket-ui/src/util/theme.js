export default {
  palette: {
    primary: {
      light: "#b6ffff",
      main: "#46C5E2",
      dark: "#BAE6F8",
      contrastText: "#fff",
    },
    secondary: {
      light: "#ff6333",
      main: "#fff",
      dark: "#b22a00",
      contrastText: "#fff",
    },
  },
  typography: {
    useNextVariants: true,
    fontFamily: ["Josefin Sans", "Open Sans"],
  },
  cardHeader: {
    backgroundColor: "#db612e",
    color: "#fff",
    padding: "3px",
    boxShadow: "5px 10px 8px #D6D5D5",
  },
  form: {
    display: "flex",
    textAlign: "center",
    backgroundColor: "#f8f9fa",
  },
  image: {
    margin: "20px auto 20px auto",
    height: 145,
    width: 145,
    borderRadius: "50%",
    boxShadow: "0px 5px 5px rgba(0,0,0,0.2)",
  },
  chip: {
    margin: "9px",
  },
  column: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    margin: 3,
  },
  rows: {
    display: "flex",
    flexDirection: "row",
    margin: "9px",
  },
  pageTitle: {
    margin: "10px auto 10px auto",
  },
  textField: {
    margin: "10px auto 10px auto",
  },
  button: {
    marginTop: 18,
    marginBottom: 18,
    position: "relative",
  },
  customError: {
    color: "red",
    fontSize: "0.8rem",
    marginTop: 10,
  },
  progress: {
    position: "absolute",
  },
  invisibleSeparator: {
    border: "none",
    margin: 4,
  },
  visibleSeparator: {
    width: "100%",
    borderBottom: "1px solid rgba(0,0,0,0.1)",
    marginBottom: 20,
  },
  paper: {
    padding: 20,
  },
  profile: {
    "& .image-wrapper": {
      padding: 9,
      textAlign: "center",
      position: "relative",
      "& button": {
        position: "absolute",
        top: "80%",
        left: "70%",
      },
    },
    "& .profile-image": {
      width: 200,
      height: 200,
      objectFit: "cover",
      maxWidth: "100%",
      borderRadius: "50%",
    },
    "& .profile-details": {
      textAlign: "center",
      "& span, svg": {
        verticalAlign: "middle",
      },
      "& a": {
        color: "#00bcd4",
      },
    },
    "& hr": {
      border: "none",
      margin: "0 0 10px 0",
    },
    "& svg.button": {
      "&:hover": {
        cursor: "pointer",
      },
    },
  },
  buttons: {
    textAlign: "center",
    "& a": {
      margin: "20px 10px",
    },
  },
};
