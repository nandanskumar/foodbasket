import React, { Component } from "react";
import { Router, Route, Switch } from "react-router-dom";
import "./App.css";


import { Provider } from "react-redux";
import store from "./store/store";

import Login from "./components/login";
import Home from "./components/home";

import history from "./util/history";
import Navbar from "./util/navbar";


class App extends Component {
  render() {
    return (
        <Provider store={store}>
          <Router history={history}>
            <Navbar />
            <div className="container">
              <Switch>
                <Route exact path="/" component={Login} />
                <Route exact path="/home" component={Home} />
              </Switch>
            </div>
          </Router>
        </Provider>
    );
  }
}

export default App;
